<?php
/**
 * Plugin Name: Simple Visitor Tracker
 * Plugin URI: www.nasirahmed.com/simple-visitor-tracker
 * Version: 0.5
 * Author: Nasir Ahmed
 * Author URI: www.nasirahmed.com
 */


define('NA_SVT_MAIN_FILE', __FILE__);
define('NA_SVT_ROOT_DIR', dirname(__FILE__));


function svt_scripts_enqueue() {
    wp_register_script( 'svt-script-js', plugins_url('/js/script.js', NA_SVT_MAIN_FILE));
    wp_register_style( 'svt-style-css', plugins_url('/css/style.css', NA_SVT_MAIN_FILE));

    wp_enqueue_script( 'svt-script-js' );
    wp_enqueue_style( 'svt-style-css' );
}

add_action( 'admin_enqueue_scripts', 'svt_scripts_enqueue' );
add_action('admin_menu', 'sv_tracker_admin_menu');

function sv_tracker_admin_menu(){
    add_menu_page('Simple Visitor Tracker', 'SV Tracker', 'manage_options', 'simple-visitor-tracker', 'svt_init');
}


//Disclaimer about ip address and location. We collected visitor's IP Address supplied by http request. Remember that this can be fooled by visitors if he is smart.

function svt_init(){

    global $wpdb;
    $results = $wpdb->get_results('SELECT * FROM wp_sv_tracker ORDER BY time DESC LIMIT 50');
    ?>
        <div id="svt-main-area">
            <table>
            <tr>
                <th>Time</th>
                <th>IP Adress</th>
                <th>Location</th>
                <th>Brower</th>
                <th>Platform</th>
            </tr>
            <?php
            foreach($results as $result){
            ?>
            <tr>
                <td><?php echo $result->time; ?></td>
                <td><?php echo $result->ip_address; ?></td>
                <td><?php echo $result->city . ", ". $result->country; ?></td>
                <td><?php echo $result->browser_name ." ". $result->browser_version; ?></td>
                <td><?php echo $result->platform; ?></td>
            </tr>
            <?php
                }
            ?>
        </table>
        </div>
    <?php
}



//create table while install
global $svt_db_version;
$svt_db_version = "1.0";

function svt_install(){
    global $wpdb;
    global $svt_db_version;

    $table_name = $wpdb->prefix. "sv_tracker";

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "
        CREATE TABLE $table_name (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                browser_name VARCHAR(30),
                browser_version VARCHAR(100),
                ip_address VARCHAR(50),
                platform VARCHAR(100),
                user_agent VARCHAR(300),
                city VARCHAR(100),
                country VARCHAR(50),
                time DATETIME,
                UNIQUE KEY id (id)
            ) $charset_collate;
    ";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta( $sql );

    add_option('svt_db_version', $svt_db_version);
}

function svt_install_data(){
    $welcome_name = 'Mr. Wordpress';
    $welcome_text = 'Congratulations you just completed the installation!';
}

function svt_uninstall(){
    global $wpdb;

    $wpdb->query("DROP TABLE IF EXISTS wp_sv_tracker");
}

register_activation_hook(__FILE__, 'svt_install');
register_activation_hook(__FILE__, 'svt_install_data');
register_deactivation_hook( __FILE__, 'svt_uninstall' );


require_once 'BrowserDetection.php';

$broDet = new BrowserDetection();

$browser = $broDet->getBrowser();
$browser_version = $broDet->getVersion();
$ip = $broDet->getRealIpAddr();
//$unique_ip = checkUniqueVisitor($ip);
$platform = $broDet->getPlatform();
$user_agent = $broDet->getUserAgent();
$location = $broDet->getLocation('103.16.154.50');
$city = $location->city;
$country = $location->country;

if(isset($wpdb) && check_unique_visitor($ip)){
    $wpdb->insert('wp_sv_tracker', array(
        'browser_name' => $browser,
        'browser_version' => $browser_version,
        'ip_address' => $ip,
        'platform' => $platform,
        'user_agent' => $user_agent,
        'city' => $city,
        'country' => $country,
        'time' => current_time('mysql')
    ));
}


function check_unique_visitor($ip){
    global $wpdb;
    $result = $wpdb->get_row("SELECT ip_address FROM wp_sv_tracker WHERE ip_address = '$ip'");

    if($result == NULL)
        return true;
}